package com.bunifu.ipctest.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;


import java.util.Random;

public class RandGenSyncService extends Service {
    public RandGenSyncService() {
    }

    private final ITestService.Stub mBinder = new ITestService.Stub(){

        @Override
        public int getRand() throws RemoteException {
            return randNoGenerator();
        }
    };

    public int randNoGenerator(){
        return new Random().nextInt(100);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}
