package com.bunifu.ipctest.sync;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bunifu.ipctest.R;
import com.bunifu.ipctest.async.AsynchronousActivity;

public class SynchronousActivity extends AppCompatActivity {

    /*
    * In this case the caller blocks while waiting for the response.
    * Its synchronous.
    * Not multi-threaded
   */

    ITestService iTestService = null;



    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, RandGenSyncService.class);
        startService(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synchronous);

        Button mButton = (Button)findViewById(R.id.get_rand);
        final TextView mTextView = (TextView)findViewById(R.id.text_holder);

        ServiceConnection mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                iTestService = ITestService.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                iTestService = null;
            }
        };

        Intent intent = new Intent(this, RandGenSyncService.class);
        intent.setAction(ITestService.class.getName());
        bindService(intent,mServiceConnection, Context.BIND_AUTO_CREATE );

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    //Toast.makeText(SynchronousActivity.this,,Toast.LENGTH_LONG).show();

                    mTextView.setText(String.valueOf(iTestService.getRand()));

                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        Button mButtonNext = (Button)findViewById(R.id.btn_next);
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SynchronousActivity.this, AsynchronousActivity.class);
                startActivity(intent);
            }
        });

    }

}
