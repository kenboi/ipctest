package com.bunifu.ipctest.async;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import java.util.Random;

public class RandGenAsyncService extends Service {
    public RandGenAsyncService() {
    }

    private final IRandGen.Stub iBinder = new IRandGen.Stub() {
        @Override
        public void setCallBack(IRandGenCallBack callback) throws RemoteException {
            callback.sendRand(randNoGenerator());
        }
    };

    public int randNoGenerator(){
        return new Random().nextInt(100);
    }

    @Override
    public IBinder onBind(Intent intent) {
       return iBinder;
    }
}
