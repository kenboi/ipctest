package com.bunifu.ipctest.async;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bunifu.ipctest.R;
import com.bunifu.ipctest.sync.SynchronousActivity;

public class AsynchronousActivity extends AppCompatActivity {
    /*
    *
    * The caller does not block for responses
    * its asynchronous and multi-threaded
    * more stylizes
    * statically typed
    */

    IRandGen iRandGen = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asynchronous);

        Button mGetRand = (Button) findViewById(R.id.btn_get_rand);
        Button mPrevious = (Button) findViewById(R.id.btn_previous);
        final TextView mRandNo = (TextView) findViewById(R.id.txt_randNo);


        final IRandGenCallBack callBack = new IRandGenCallBack.Stub() {
            @Override
            public void sendRand(final int key) throws RemoteException {

                mRandNo.post(new Runnable() {
                    @Override
                    public void run() {
                        mRandNo.setText(String.valueOf(key));
                    }
                });
            }
        };

        ServiceConnection mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                iRandGen = IRandGen.Stub.asInterface(service);

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                iRandGen = null;
            }
        };

        Intent intent = new Intent(this, RandGenAsyncService.class);
        intent.setAction(IRandGen.class.getName());
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

        mGetRand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    iRandGen.setCallBack(callBack);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        mPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AsynchronousActivity.this, SynchronousActivity.class));
            }
        });

    }


}
