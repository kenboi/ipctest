// IRandGenCallBack.aidl
package com.bunifu.ipctest.async;

// Declare any non-default types here with import statements

interface IRandGenCallBack {
    //Sends the generated rand no back to the client
    oneway void sendRand(in int key);
}
