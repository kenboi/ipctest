// IRandGen.aidl
package com.bunifu.ipctest.async;

// Declare any non-default types here with import statements
import com.bunifu.ipctest.async.IRandGenCallBack;
interface IRandGen {
    //Takes a reference to a key geneartor callback as a in parameter
    oneway void setCallBack(in IRandGenCallBack callback);
}
