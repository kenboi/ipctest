// ITestService.aidl
package com.bunifu.ipctest.sync;

// Declare any non-default types here with import statements

interface ITestService {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
   int getRand();
}
