# Inter Process communication - Android #

### Quick intro ###
So i have been working on this project where i really needed to implement some kind of inter process communication a lot. So i did my homework a little research here and there. I must admit it was really hard for to get to comprehend ipc using the iBinder framework. Finally when i did manage i decided to put up this repo.
 
### What it does ###
 This is a basic App that has a implements synchronous(with blocking) and asynchronous(no blocking) communication. It has a service which generates random numbers in a service which are then sent back to the activity which updates the UI.

***This repo is meant for guys who have some basic knowledge about IPC implementation using AIDL and the iBinder framework, and need an example to drive the point home.***